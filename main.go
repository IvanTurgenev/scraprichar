package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	// "time"

	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/gocolly/colly/v2"
)

var websiteURL = "https://www.pricecharting.com/game/"

var excelFile *excelize.File
var good int
var bad int

var allGames [][]string

type lastgb struct {
	Bad  int
	Good int
}

type game struct {
	ID              string
	System          string
	Title           string
	LoosePrice      string
	CompletePrice   string
	NewPrice        string
	GradedPrice     string
	BoxOnlyPrice    string
	ManualOnlyPrice string
}

func parseTitle(s string, g game) game {
	temps := strings.Split(s, "\n")
	tempTitle := strings.TrimSpace(temps[0])
	tempSystem := strings.TrimSpace(temps[len(temps)-1])
	return game{
		Title:  tempTitle,
		System: tempSystem,
	}

}

func parsePrice(s string) string {
	temps := strings.Split(s, "\n")
	tempPrice := strings.TrimSpace(temps[0])
	return tempPrice

}

func visit(gennum chan int, writeChan chan game, printChan chan lastgb) {
	var isgood bool
	var tempGame game
	// var to = start
	var last lastgb

	collector := colly.NewCollector()
	collector.OnHTML("body", func(e *colly.HTMLElement) {
		tempGame = parseTitle(e.ChildText(`h1[id="product_name"]`), tempGame)
		tempGame.ID = e.ChildAttr(`h1[id="product_name"]`, "title")
		tempGame.LoosePrice = e.ChildText(`span[title="current value in used condition"]`)
		tempGame.CompletePrice = e.ChildText(`span[title="current value in Complete (CIB) condition"]`)
		tempGame.NewPrice = e.ChildText(`span[title="current value in New condition"]`)
		tempGame.GradedPrice = parsePrice(e.ChildText(`span[title="current value in Graded condition"]`))
		tempGame.BoxOnlyPrice = parsePrice(e.ChildText(`span[title="current Box Only value"]`))
		tempGame.ManualOnlyPrice = parsePrice(e.ChildText(`span[title="current Manual Only value"]`))
		isgood = true
	})
	for {
		num := <-gennum
		prourl := websiteURL + strconv.Itoa(num)
		collector.Visit(prourl)
		if isgood {
			isgood = false
			last.Good = num
			writeChan <- tempGame
		} else {
			last.Bad = num

		}
		printChan <- last
	}

}

func getFileWriter(writeChan chan game) {
	excelFile = excelize.NewFile()
	excelFile.NewSheet("Sheet1")
	var headers = []string{"Title", "System", "ID", "LoosePrice", "CompletePrice", "NewPrice", "GradedPrice", "BoxOnlyPrice", "ManualOnlyPrice"}
	excelFile.SetSheetRow("Sheet1", "A1", &headers)
	var i int = 1
	for {
		gamerow := <-writeChan

		row := []string{gamerow.Title, gamerow.System, gamerow.ID, gamerow.LoosePrice, gamerow.CompletePrice, gamerow.NewPrice, gamerow.GradedPrice, gamerow.BoxOnlyPrice, gamerow.ManualOnlyPrice}
		excelFile.SetSheetRow("Sheet1", "A"+strconv.Itoa(i+1), &row)
		i++
	}
}

func chanPrint(writechan chan lastgb) {
	for {
		last := <-writechan
		if last.Good > good {
			good = last.Good
		}
		if last.Bad > bad {
			bad = last.Bad
		}
		var goodS = strconv.Itoa(good)
		var badS = strconv.Itoa(bad)
		fmt.Printf("\033[2K\r%s", "Last good ID: "+goodS+" | Last bad ID: "+badS)
	}
}

func numberGenerator(start int, genNum chan int) {
	var i int = start
	for {
		genNum <- i
		i++
	}

}

func unique(intSlice []string) []string {
	keys := make(map[string]bool)
	list := []string{}
	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func writeSystemFiles() {
	var systems []string
	for _, row := range allGames[1:] {
		if row[1] == "" {

		} else {
			systems = append(systems, row[1])

		}
	}
	systems = unique(systems)
	// dt := time.Now()
	for _, system := range systems {
		var filname = strings.Replace(system, " ", "", -1) + ".xlsx"

		systemsByWorksheet := excelize.NewFile()
		var headers = []string{"Title", "System", "ID", "LoosePrice", "CompletePrice", "NewPrice", "GradedPrice", "BoxOnlyPrice", "ManualOnlyPrice"}
		systemsByWorksheet.SetSheetRow("Sheet1", "A1", &headers)
		var i int = 1
		for _, row := range allGames {
			if row[1] == system {

				systemsByWorksheet.SetSheetRow("Sheet1", "A"+strconv.Itoa(i+1), &row)
				i++
			}
		}
		if err := systemsByWorksheet.SaveAs(filname); err != nil {
			fmt.Println(err)

		}
	}
	// systemsByWorksheet.NewSheet(sheet)
	// if err := systemsByWorksheet.Save(); err != nil {
	// 	fmt.Println(err)
	// }
}

func readGames() {
	gamesFile, err := excelize.OpenFile("games.xlsx")
	if err != nil {
		fmt.Println(err)
		return
	}
	rows := gamesFile.GetRows("Sheet1")
	for _, row := range rows {
		allGames = append(allGames, row)
		// for _, colCell := range row {
		// 	fmt.Println(colCell[2])
		// }
		// fmt.Println()
	}
	err = gamesFile.Save()
	if err != nil {
		fmt.Println(err)
	}
	// fmt.Println(unique(systems))
	// for _, system := range unique(systems) {
	// systemsByWorksheet.SetCellValue(system, "A1", "Hello world.")

	// }

	// var headers = []string{"Title", "System", "ID", "LoosePrice", "CompletePrice", "NewPrice", "GradedPrice", "BoxOnlyPrice", "ManualOnlyPrice"}
	// excelFile.SetSheetRow("Sheet1", "A1", &headers)
	// var i int = 1
	// for {
	// 	// gamerow := <-writeChan

	// 	// row := []string{gamerow.Title, gamerow.System, gamerow.ID, gamerow.LoosePrice, gamerow.CompletePrice, gamerow.NewPrice, gamerow.GradedPrice, gamerow.BoxOnlyPrice, gamerow.ManualOnlyPrice}
	// 	// excelFile.SetSheetRow("Sheet1", "A"+strconv.Itoa(i+1), &row)
	// 	i++
	// }
	// if err = gamesFile.Save(); err != nil {
	//     fmt.Println(err)
	// }
}

func main() {
	fmt.Println("Options")
	fmt.Println("filter")
	fmt.Println("When filtering rename file to filter to games.xlsx")
	fmt.Println("scrape idToStart")
	fmt.Println("Example: scrape 1")
	fmt.Println("Example: scrape 12000")
	fmt.Println("Example: filter games.xlsx")
	args := os.Args[1:]
	if args[0] == "filter" {
		fmt.Println("Filtering")
		readGames()
		writeSystemFiles()
	} else if args[0] == "scrape" {
		fmt.Println("Scraping...")
		fmt.Println("Ctrl + c to stop")
		printChan := make(chan lastgb)
		writeChan := make(chan game)
		genNum := make(chan int)
		var numworkers int = 2
		var start int = 1
		go numberGenerator(start, genNum)
		go getFileWriter(writeChan)

		fmt.Println("Workers starting: " + strconv.Itoa(numworkers))
		for i := 0; i < numworkers; i++ {
			go visit(genNum, writeChan, printChan)
		}
		go chanPrint(printChan)
		done := make(chan struct{})

		go func() {
			c := make(chan os.Signal, 1) // we need to reserve to buffer size 1, so the notifier are not blocked
			signal.Notify(c, os.Interrupt, syscall.SIGTERM)

			<-c
			close(done)
		}()

		<-done
		// dt := time.Now()
		var filename = "games-" + strconv.Itoa(start) + "-" + strconv.Itoa(good) + ".xlsx"
		if err := excelFile.SaveAs(filename); err != nil {
			fmt.Println(err)
		}
		log.Println("Saving to: " + filename)
		log.Println("Exiting...")
	}
	fmt.Println("No arguments...: quitting")
}
