module gitlab.com/IvanTurgenev/scraprichar

go 1.14

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/gocolly/colly/v2 v2.1.0
	google.golang.org/api v0.29.0
)
